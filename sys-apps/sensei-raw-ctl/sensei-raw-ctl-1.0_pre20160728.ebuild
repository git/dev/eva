# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
EGIT_COMMIT="72fb3369c7908b94f42026b0c551c225de6fe112"

inherit cmake-utils gnome2-utils vcs-snapshot xdg

DESCRIPTION="SteelSeries Sensei Raw configuration tool"
HOMEPAGE="https://github.com/pjanouch/sensei-raw-ctl"
SRC_URI="https://github.com/pjanouch/${PN}/archive/${EGIT_COMMIT}.tar.gz -> ${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
IUSE="gtk"

RDEPEND="
	virtual/libusb:1
	gtk? ( x11-libs/gtk+:3 )
"
DEPEND="${RDEPEND}
	sys-apps/help2man
	virtual/pkgconfig
"

src_configure() {
	local mycmakeargs=(
		-DBUILD_GUI=$(usex gtk)
	)

	cmake-utils_src_configure
}

pkg_preinst() {
	gnome2_icon_savelist
	xdg_pkg_preinst
}

pkg_postinst() {
	gnome2_icon_cache_update
	xdg_pkg_postinst
}

pkg_postrm() {
	gnome2_icon_cache_update
	xdg_pkg_postrm
}
