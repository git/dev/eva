# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="high performance HTTP server that implements restic's REST backend API"
HOMEPAGE="https://github.com/restic/rest-server"
SRC_URI="https://github.com/restic/rest-server/archive/v${PV}.tar.gz -> ${P}.tar.gz
	https://www.whyte.ninja/pub/${P}-deps.tar.xz"
S="${WORKDIR}/rest-server-${PV}"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="${DEPEND}"

src_compile() {
	local mygoargs=(
		-tags release
		-ldflags "-X main.version=${PV}"
		-asmflags "-trimpath=${S}"
		-gcflags "-trimpath=${S}"
	)

	ego build "${mygoargs[@]}" -o rest-server ./cmd/rest-server
}

src_install() {
	dobin rest-server
	dodoc AUTHORS CHANGELOG.md README.md
	# TODO: install systemd unit/socket
}
