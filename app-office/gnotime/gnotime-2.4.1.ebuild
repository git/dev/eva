# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"
GCONF_DEBUG="yes"

inherit autotools eutils gnome2

DESCRIPTION="Utility to track time spent on activities"
HOMEPAGE="http://gttr.sourceforge.net/"
SRC_URI="mirror://sourceforge/gttr/${P}.tar.gz"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~amd64 ~ppc ~ppc64 ~sparc ~x86"
IUSE=""

RDEPEND="
	>=dev-libs/dbus-glib-0.74
	>=dev-libs/glib-2.14:2
	dev-libs/libxml2:2
	dev-libs/qof:2
	>=dev-scheme/guile-2.0:2
	>=gnome-base/gconf-2:2
	>=gnome-base/gnome-vfs-2:2
	>=gnome-base/libglade-2:2.0
	>=gnome-base/libgnome-2
	>=gnome-base/libgnomeui-2.3
	>=gnome-extra/gtkhtml-3.14.3:3.14
	>=x11-libs/gtk+-2.12:2
	x11-libs/libX11
	x11-libs/libXScrnSaver
	x11-libs/pango
"
DEPEND="${RDEPEND}
	app-text/docbook-xml-dtd:4.2
	>=app-text/scrollkeeper-0.3.11
	>=dev-util/intltool-0.35.5
	sys-devel/gettext
	virtual/pkgconfig
	x11-proto/scrnsaverproto
"

src_prepare() {
	# Fix linking of gnotime binary
	epatch "${FILESDIR}"/${P}-link.patch

	eautoreconf
	gnome2_src_prepare
}
