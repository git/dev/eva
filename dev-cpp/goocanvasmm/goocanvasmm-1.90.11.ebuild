# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"
GCONF_DEBUG="no"

inherit gnome2

DESCRIPTION="C++ bindings for x11-libs/goocanvas"
HOMEPAGE="http://live.gnome.org/GooCanvas"

LICENSE="LGPL-2.1"
SLOT="2.0"
KEYWORDS="~amd64"
IUSE="static-libs"

RDEPEND="
	>=dev-cpp/glibmm-2.46.1:2
	>=dev-cpp/gtkmm-3.18:3.0
	>=x11-libs/goocanvas-2.0.1:2.0
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
"

# FIXME: requires c++11 support

src_configure() {
	gnome2_src_configure $(use_enable static-libs static)
}
