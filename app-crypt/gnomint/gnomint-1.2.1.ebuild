# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"
GCONF_DEBUG="no"

inherit autotools eutils gnome2

DESCRIPTION="A Certification Authority Software for Gnome"
HOMEPAGE="http://gnomint.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

SLOT="0"
LICENSE="GPL-3"
KEYWORDS="~amd64 ~x86"
IUSE="utils"

# IUSE=debug is most likely useless, -O0 -g
COMMON_DEPEND=">=net-libs/gnutls-2.7.4
	>=dev-db/sqlite-3:3
	>=dev-libs/glib-2.6:2
	>=gnome-base/gconf-2
	>=x11-libs/gtk+-2.12:2
	>=app-text/iso-codes-0.35
	sys-libs/readline
"
RDEPEND="${COMMON_DEPEND}
	utils? ( dev-db/sqlite:0 )
"
DEPEND="${COMMON_DEPEND}
	>=dev-util/intltool-0.23
	virtual/pkgconfig
"

src_prepare() {
	DOCS="AUTHORS BUGS ChangeLog MAINTAINERS NEWS README TODO"

	# Fix intltool tests
	echo "src/country_table.c" >> po/POTFILES.in

	# Do not build released software with stupid CFLAGS
	sed -e 's/-Werror//' \
		-e 's/-g -O0//' \
		-i configure.in configure || die

	# Fix build due to missing gcrypt in LDADD/CFLAGS
	epatch "${FILESDIR}/${P}-add-missing-gcrypt.patch"

	eautoreconf
	gnome2_src_prepare
}

src_install() {
	gnome2_src_install
	use utils || rm "${D}"/usr/bin/gnomint-upgrade-db
}
