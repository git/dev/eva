# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"
GCONF_DEBUG="no"
PYTHON_COMPAT=( python3_4 )

inherit gnome2 python-r1

DESCRIPTION="User-friendly database application"
HOMEPAGE="http://www.glom.org"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="mysql postgres"

RDEPEND="
	>=app-arch/libarchive-3
	>=dev-libs/glib-2.24:2
	>=dev-cpp/glibmm-2.46.1
	>=dev-cpp/libxmlpp-2.24
	>=dev-libs/libxslt-1.1.10
	>=dev-python/pygobject-2.29:3
	>=dev-cpp/libgdamm-4.99.10:5
	>=gnome-extra/libgda-5.2.1:5[postgres?,mysql?]
	>=dev-cpp/gtkmm-3.18:3.0
	>=net-libs/libepc-0.4
	>=dev-cpp/goocanvasmm-1.90.11:2.0
	>=app-text/evince-3.0
	app-text/iso-codes
	>=dev-cpp/gtksourceviewmm-3.18:3.0
	dev-libs/boost[python]
	>=x11-libs/goocanvas-2.0.1
	mysql? ( dev-db/mysql )
	postgres? ( dev-db/postgresql:= )
"
# FIXME: is postgres-server really needed ?
DEPEND="${RDEPEND}
	app-text/yelp-tools
	>=dev-util/intltool-0.35
	>=sys-devel/gettext-0.17
	dev-python/sphinx
"

# FIXME: needs gcc with c++11 support

pkg_setup() {
	python-r1_pkg_setup
}

src_configure() {
	gnome2_src_configure \
		--disable-update-mime-database \
		--disable-scrollkeeper \
		--disable-client-only \
		--enable-glom-ui \
		--enable-sqlite \
		$(use_enable mysql) \
		$(use_enable postgres postgresql)
}

src_install() {
	DOCS="AUTHORS ChangeLog NEWS README TODO"
	gnome2_src_install
}
