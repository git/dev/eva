# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit ltprune systemd user

DESCRIPTION="Serve DAAP (iTunes) and DPAP (iPhoto) shares from Linux"
HOMEPAGE="http://www.flyn.org/projects/dmapd/"
SRC_URI="http://www.flyn.org/projects/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="test"

RDEPEND="
	>=dev-libs/glib-2.36:2
	media-libs/libexif
	>=net-libs/libdmapsharing-2.9.21:3.0
	|| (
		media-gfx/graphicsmagick
		media-gfx/imagemagick )
	>=media-libs/vips-7.38:0
	media-libs/gstreamer:1.0
	|| (
		sys-libs/db:4.8
		sys-libs/db:4.7 )
"
DEPEND="${RDEPEND}
	>=dev-libs/check-0.9.4
	virtual/pkgconfig
"

DOCS="AUTHORS ChangeLog NEWS README* TODO"

pkg_setup() {
	enewgroup dmapd
	enewuser dmapd -1 -1 -1 dmapd
}

src_configure() {
	econf \
		--disable-static \
		--localstatedir=/var \
		$(use_enable test tests) \
		$(systemd_with_unitdir)
}

src_install() {
	default
	prune_libtool_files

	insinto /etc
	doins "${S}"/distro/dmapd.conf

	keepdir /var/cache/dmapd/DAAP
	keepdir /var/cache/dmapd/DPAP

	fowners dmapd:root /var/cache/dmapd
}
