# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit games

DESCRIPTION="gbrainy is a brain teaser game and trainer to have fun and to keep your brain trained."
HOMEPAGE="http://live.gnome.org/gbrainy"
SRC_URI="http://gent.softcatala.org/jmas/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="debug plugins"

RDEPEND="dev-libs/glib:2
	>=dev-lang/mono-1.1.7
	>=dev-dotnet/gtk-sharp-2.10:2
	>=x11-libs/gtk+-2.8:2
	>=gnome-base/librsvg-2.2
	plugins? ( >=dev-dotnet/mono-addins-0.3 )"
DEPEND="${DEPEND}
	>=dev-util/intltool-0.35
	app-text/gnome-doc-utils"

src_configure() {
	econf $(use_enable plugins addins) $(use_enable debug)
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	prepgamesdirs
}
