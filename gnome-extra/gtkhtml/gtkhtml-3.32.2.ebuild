# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
GNOME_TARBALL_SUFFIX="bz2"

inherit gnome2

DESCRIPTION="Lightweight HTML rendering/printing/editing engine"
HOMEPAGE="http://projects.gnome.org/evolution/"

LICENSE="GPL-2+ LGPL-2+"
SLOT="3.14"
KEYWORDS="~alpha ~amd64 ~arm ~ia64 ~ppc ~ppc64 ~sh ~sparc ~x86"
IUSE=""

RDEPEND="
	>=app-text/enchant-1.1.7
	>=app-text/iso-codes-0.49
	>=dev-libs/glib-2.24:2
	gnome-base/gconf:2
	>=gnome-base/orbit-2
	>=net-libs/libsoup-2.26.0:2.4
	>=x11-libs/gtk+-2.20:2
	>=x11-themes/gnome-icon-theme-2.22.0
"
DEPEND="${RDEPEND}
	>=dev-util/intltool-0.40.0
	sys-devel/gettext
	virtual/pkgconfig
	x11-proto/xproto
"

PATCHES=(
	"${FILESDIR}"/${P}-g_thread_init.patch
)

src_configure() {
	gnome2_src_configure \
		--disable-static \
		--disable-deprecated-warning-flags
}
