# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=4

inherit eutils

DESCRIPTION="A Query Object Framework"
HOMEPAGE="http://qof.alioth.debian.org/"
SRC_URI="mirror://sourceforge/qof/${P}.tar.gz"
LICENSE="GPL-2"

SLOT="0"

KEYWORDS="amd64 ~ppc ~ppc64 sparc x86"

IUSE="doc nls sqlite"

RDEPEND="
	dev-libs/libxml2:2
	dev-libs/glib:2
	sqlite? ( dev-db/sqlite:0 )
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	!dev-libs/qof:2
	doc? ( app-doc/doxygen )"

src_prepare() {
	# Upstream not willing to remove those stupid flags...
	epatch "${FILESDIR}/${PN}-0.7.4-remove_spurious_CFLAGS.patch"
}

src_configure() {
	econf \
		--disable-error-on-warning \
		--disable-dot \
		--disable-gdabackend \
		--disable-gdasql \
		$(use_enable doc doxygen) \
		$(use_enable doc html-docs) \
		$(use_enable nls) \
		$(use_enable sqlite)
}
